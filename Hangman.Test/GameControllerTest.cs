﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Moq;
using Hangman.Controllers;
using Hangman.Models;
using System.Web.Mvc;
using System.Net.Mail;
using Hangman.Entities;
using Hangman.Services;

namespace Hangman.Test
{
    public class GameControllerTest
    {
        public class Authenticate
        {
            [Fact]
            public void AuthenticateSetsCookieIfSuccessful()
            {
                var game = TestableGameController.Create();
                game.UserService.Setup(a => a.Validate("user", "pass")).Returns(true);

                game.Authenticate("user", "pass");

                game.AuthenticationService.Verify(a => a.SetAuthCookie("user", true));
            }

            [Fact]
            public void AuthenticateRedirectsIfSuccessful()
            {
                var game = TestableGameController.Create();
                game.UserService.Setup(a => a.Validate("user", "pass")).Returns(true);

                var result = game.Authenticate("user", "pass") as RedirectToRouteResult;

                Assert.Equal("Default", result.RouteName);
            }

            [Fact]
            public void AuthenticateFailReturnsPageWithErrors()
            {
                var game = TestableGameController.Create();
                game.UserService.Setup(a => a.Validate("user", "pass")).Returns(false);

                var result = game.Authenticate("user", "pass") as ViewResult;

                Assert.Equal("Index", result.ViewName);
                Assert.Equal("Wrong user or password.", (result.Model as GameViewModel).ErrorMessage);
            }
        }

        public class GetChallenges
        {
            [Fact]
            public void CreatorIsSetIfChallengeWasCreatedByCurrentUser()
            {
                var game = TestableGameController.Create();
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                game.Repo.Setup(r => r.GetAll()).Returns(new List<Challenge>() { new Challenge() { Username = "user1" } }.AsQueryable());

                var result = game.Challenges() as JsonResult;
                var data = (result.Data as IEnumerable<ChallengeViewModel>).First();
                Assert.True(data.isCreator);
            }

            [Fact]
            public void ReturnsNextPageIfStartAndMaxResultsAreProvided()
            {
                var game = TestableGameController.Create();
                var challenges = new List<Challenge>()
                {
                    new Challenge(){ Id = 0 },
                    new Challenge(){ Id = 1 },
                    new Challenge(){ Id = 2 },
                    new Challenge(){ Id = 3 }
                };

                game.Repo.Setup(r => r.GetAll()).Returns(challenges.AsQueryable());
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                var result = game.Challenges(2, 2) as JsonResult;
                var data = result.Data as IEnumerable<ChallengeViewModel>;

                Assert.Equal(2, data.Count());
                Assert.Equal(2, data.ElementAt(0).id);
                Assert.Equal(3, data.ElementAt(1).id);
            }

            [Fact]
            public void ChallengeIsUnansweredIfNoChallengeResultExistsForCurrentUser()
            {
                var game = TestableGameController.Create();

                game.Repo.Setup(r => r.GetAll()).Returns(new List<Challenge>() { new Challenge() }.AsQueryable());
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                var result = game.Challenges() as JsonResult;
                var model = ((IEnumerable<ChallengeViewModel>)result.Data).First();

                Assert.False(model.answered);
                Assert.False(model.correct);
            }

            [Fact]
            public void ChallengeIsAnsweredIfCreatedByCurrentUser()
            {
                var game = TestableGameController.Create();
                var challenge = new Challenge() { Username = "user1" };
                game.Repo.Setup(r => r.GetAll()).Returns(new List<Challenge>() { challenge }.AsQueryable());
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                var result = game.Challenges() as JsonResult;
                
                var model = ((IEnumerable<ChallengeViewModel>)result.Data).First();
                Assert.True(model.answered);
                Assert.True(model.correct);
            }

            [Fact]
            public void ChallengeIsAnsweredIfChallengeResultExistsForCurrentUser()
            {
                var game = TestableGameController.Create();

                var challenge = new Challenge();
                challenge.Results.Add(new ChallengeResult() { Username = "user1" });
                game.Repo.Setup(r => r.GetAll()).Returns(new List<Challenge>() { challenge }.AsQueryable());
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                var result = game.Challenges() as JsonResult;
                var challengeModel = ((IEnumerable<ChallengeViewModel>)result.Data).First();

                Assert.True(challengeModel.answered);
                Assert.False(challengeModel.correct);
            }

            [Fact]
            public void ChallengeIsCorrectIfChallengeResultForCurrentUserIsCorrect()
            {
                var game = TestableGameController.Create();

                var challenge = new Challenge();
                challenge.Results.Add(new ChallengeResult() { Username = "user1", Correct = true });
                game.Repo.Setup(r => r.GetAll()).Returns(new List<Challenge>() { challenge }.AsQueryable());
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                var result = game.Challenges() as JsonResult;
                var challengeModel = ((IEnumerable<ChallengeViewModel>)result.Data).First();

                Assert.True(challengeModel.answered);
                Assert.True(challengeModel.correct);
            }

            [Fact]
            public void ChallengeContainsAllResults()
            {
                var game = TestableGameController.Create();
                var challenge = new Challenge();
                challenge.Results.Add(new ChallengeResult() { Username = "user1", Correct = true });
                game.Repo.Setup(r => r.GetAll()).Returns(new List<Challenge>() { challenge }.AsQueryable());
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                var result = game.Challenges() as JsonResult;
                var challengeModel = ((IEnumerable<ChallengeViewModel>)result.Data).First();

                Assert.Equal(1, challengeModel.results.Length);
            }
        }

        public class PostResult
        {
            [Fact]
            public void IfChallengeDoesNotExistShouldThrowError()
            {
                var game = TestableGameController.Create();
                var challengeResult = new ChallengeResultViewModel() { challengeId = 1, correct = true };
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                Assert.Throws(typeof(InvalidOperationException), () => game.ChallengeResults(challengeResult));
            }

            [Fact]
            public void AddsResultForCurrentUser()
            {
                var game = TestableGameController.Create();
                var challengeResult = new ChallengeResultViewModel() { challengeId = 1, correct = true };
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                var challenge = new Challenge() { Id = 1 };
                game.Repo.Setup(r => r.GetAll()).Returns(new Challenge[1] { challenge }.AsQueryable()); 

                game.ChallengeResults(challengeResult);

                Assert.Equal(1, challenge.Results.Count);
                Assert.Equal("user1", challenge.Results.ElementAt(0).Username);
            }

            [Fact]
            public void ReturnsNewResult()
            {
                var game = TestableGameController.Create();
                var challengeResult = new ChallengeResultViewModel() { challengeId = 1, correct = true };
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                var challenge = new Challenge() { Id = 1 };
                game.Repo.Setup(r => r.GetAll()).Returns(new Challenge[1] { challenge }.AsQueryable());

                var result = game.ChallengeResults(challengeResult) as JsonResult;

                Assert.Equal("user1", ((ChallengeResultViewModel)result.Data).username);
            }

            [Fact]
            public void IfResultAlreadyExistsForCurrentUserShouldThrowError()
            {
                var game = TestableGameController.Create();
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                var challenge = new Challenge() { Id = 1 };
                challenge.Results.Add(new ChallengeResult() { Username = "user1" });
                game.Repo.Setup(r => r.GetAll()).Returns( new Challenge[1] { challenge}.AsQueryable()); 

                var challengeResult = new ChallengeResultViewModel() { challengeId = 1, correct = true };

                Assert.Throws(typeof(InvalidOperationException), () => game.ChallengeResults(challengeResult));
            }

            [Fact]
            public void IfChallengeWasCreatedByCurrentUserShouldThrowError()
            {
                var game = TestableGameController.Create();
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                var challenge = new Challenge() { Id = 1, Username = "user1" };
                game.Repo.Setup(r => r.GetAll()).Returns(new Challenge[1] { challenge }.AsQueryable());

                var challengeResult = new ChallengeResultViewModel() { challengeId = 1, correct = true };

                Assert.Throws(typeof(InvalidOperationException), () => game.ChallengeResults(challengeResult));
            }
        }

        public class PostChallenge
        {
            [Fact]
            public void SendsEmailToAllUsers()
            {
                var game = TestableGameController.Create();
                game.UserService.Setup(a => a.GetAllUserEmailAddress()).Returns(new string[1] { "test@farmas.net" });
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                MailMessage message = null;
                game.EmailService.Setup(e => e.Send(It.IsAny<MailMessage>())).Callback<MailMessage>(m => message = m);

                game.Challenges(new ChallengeViewModel() { word = "word" });

                Assert.Equal("user1 is challenging you to a word duel!", message.Subject);
                Assert.True(message.IsBodyHtml);
                Assert.Equal("fede@farmas.net", message.From.Address);
                Assert.Equal(1, message.To.Count);
                Assert.Equal("test@farmas.net", message.To[0].Address);
                Assert.Contains("___&nbsp;&nbsp;&nbsp;___&nbsp;&nbsp;&nbsp;___&nbsp;&nbsp;&nbsp;___&nbsp;&nbsp;&nbsp;", message.Body);
            }

            [Fact]
            public void CreatesChallengeWithWordInLowerCase()
            {
                var game = TestableGameController.Create();
                var challenge = new ChallengeViewModel() { word = "WORD" };
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                game.Challenges(challenge);

                game.Repo.Verify(r => r.InsertOnCommit(It.Is<Challenge>(c => c.Word == "word")));
            }

            [Fact]
            public void CreatesAChallengeForUser()
            {
                var game = TestableGameController.Create();
                var challenge = new ChallengeViewModel() { word = "word" };
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");

                game.Challenges(challenge);

                game.Repo.Verify(r => r.InsertOnCommit(It.Is<Challenge>(c => c.Username == "user1" && c.Word == "word")));
                game.Repo.Verify(r => r.CommitChanges());
            }


            [Fact]
            public void ReturnsNewChallenge()
            {
                var game = TestableGameController.Create();
                game.AuthenticationService.Setup(a => a.GetUserName()).Returns("user1");
                var challenge = new ChallengeViewModel() { word = "word" };
                
                var result = game.Challenges(challenge) as JsonResult;

                var model = result.Data as ChallengeViewModel;
                Assert.Equal("user1", model.username);
            }
        }

        [Fact]
        public void GetWord_ReturnsOneWordAtRandom()
        {
            var game = TestableGameController.Create();
            var challenges = new List<Challenge>()
            {
                new Challenge(){ Id = 0, Word = "palabra" }
            };
           
            game.Repo.Setup(r => r.GetAll()).Returns(challenges.AsQueryable());

            var result = game.Word() as JsonResult;
            Assert.Equal("{ word = palabra }", result.Data.ToString());
        }

        [Fact]
        public void LogoutEndsUserSession()
        {
            var game = TestableGameController.Create();

            var result = game.Logout() as RedirectToRouteResult;

            Assert.Equal("Default", result.RouteName);
            game.AuthenticationService.Verify(a => a.Logout());
        }
    }

    public class TestableGameController : GameController
    {
        public Mock<IEntityRepository<Challenge>> Repo;
        public Mock<IAuthenticationService> AuthenticationService;
        public Mock<IUserService> UserService;
        public Mock<IEmailService> EmailService;

        private TestableGameController(Mock<IAuthenticationService> authenticationService,
                                       Mock<IUserService> userService,
                                       Mock<IEntityRepository<Challenge>> repo,
                                       Mock<IEmailService> email)
            : base(authenticationService.Object, userService.Object, repo.Object, email.Object)
        {
            this.Repo = repo;
            this.UserService = userService;
            this.EmailService = email;
            this.AuthenticationService = authenticationService;

            userService.Setup(a => a.GetAllUserEmailAddress()).Returns(new string[1] { "foo@bar.com" });
        }

        public static TestableGameController Create()
        {
            return new TestableGameController(new Mock<IAuthenticationService>(),
                                              new Mock<IUserService>(),
                                              new Mock<IEntityRepository<Challenge>>(),
                                              new Mock<IEmailService>());
        }
    }
}