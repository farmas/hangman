﻿/// <reference path="Hangman/Content/js/Game.js" />


describe("Challenges", function () {
    var data = [
        { "id": 0, "username": "fede", "word": "lunes", "answered": true, "results": [] },
        { "id": 1, "username": "clots", "word": "martes", "answered": false, "results": [] }
    ];

    beforeEach(function () {
        this.challenges = new Hangman.Challenges(_.map(data, function (c) {
            return new Hangman.Challenge(c);
        }));
    });

    describe('firstUnsolved', function () {
        it("should return the first challenge that has not been answered", function () {
            var challenge = this.challenges.getFirstUnsolved();
            expect(challenge.get('id')).toEqual(1);
        });
    });

    describe('deselect', function () {
        it('should mark all challenges as inactive', function () {
            this.challenges.each(function (c) { c.set({ active: true }); });

            this.challenges.deselectAll();

            expect(this.challenges.every(function(c){ return c.get('active') == false; })).toBeTruthy();
        });
    });
});