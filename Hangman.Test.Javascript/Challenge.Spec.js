﻿/// <reference path="Hangman/Content/js/Game.js" />
describe("Challenge", function () {
    describe('addResult', function () {
        it('if empty, should not add results', function () {
            var challenge = new Hangman.Challenge();

            challenge.addResult(new Hangman.ChallengeResult({foo:'bar'}));

            expect(challenge.getResults().length).toEqual(0);
        });
    });
});