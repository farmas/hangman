﻿/// <reference path="Hangman/Content/js/Game.js" />
describe("During hangman app", function() {
    Hangman.Events = { };
    _.extend(Hangman.Events, Backbone.Events);

    beforeEach(function() {
        this.game = new Hangman.Game();

        Hangman.Server.getWord = function() {
            var deferred = new $.Deferred();
            deferred.resolve({ word: "word" });
            return deferred.promise();
        }
    });

    describe("when user starts any game", function() {
        it('shows initial image', function() {
            this.game.start('gato');

            expect(this.game.get('image')).toEqual('0.png');

            this.game.pressKey('z');
            this.game.start('perro');

            expect(this.game.get('image')).toEqual('0.png');
        });

        it("allows keyboard input", function() {
            this.game.set({ captureKeys: false });

            this.game.start("gato");
            expect(this.game.get('captureKeys')).toBeTruthy();
        });

        it("used letters is empty", function() {
            this.game.start("gato");
            expect(this.game.get('usedLetters').length).toEqual(0);
        });

        it("all letters are available", function() {
            this.game.start("papa");

            this.game.pressKey("p");
            this.game.pressKey("x");
            this.game.pressKey("a");

            this.game.start("gato");
            expect(this.game.get('usedLetters').length).toEqual(0);
            expect(this.game.get('state')).toEqual('play');
        });
    });

    describe("when user starts a random game", function() {
        it("loads word from server", function() {
            this.game.startRandom();
            expect(this.game.get('letters').length).toEqual(4);
        });

        it("clears the active challenge", function() {
            var challenge = new Hangman.Challenge({ word: 'papa', answered: true });

            this.game.startChallenge(challenge);
            this.game.startRandom();

            expect(challenge.get('active')).toBeFalsy();
        });
    });

    describe("when user selects a challenge", function() {
        it("is marked as active", function() {
            var challenge = new Hangman.Challenge({ word: 'papa', answered: true });

            this.game.startChallenge(challenge);

            expect(challenge.get('active')).toBeTruthy();
        });

        it("all previous selections are cleared", function() {
            var challenges = new Hangman.Challenges([
                    { word: 'papa', answered: true },
                    { word: 'mama', answered: true }]);

            var game = new Hangman.Game({ challenges: challenges });

            game.startChallenge(challenges.at(0));
            game.startChallenge(challenges.at(1));
            expect(challenges.at(0).get('active')).toBeFalsy();
        });

        it("shows answered if already solved", function() {
            var challenge = new Hangman.Challenge({ word: 'papa', answered: true, correct: true });

            this.game.startChallenge(challenge);

            expect(this.game.isOver()).toBeTruthy();
            expect(this.game.get('image')).toEqual('win.png');
        });
    });

    describe('when user presses a letter', function() {
        it('key is marked in keyboard as invisible', function() {
            this.game.start("gato");

            this.game.pressKey('g');
            this.game.pressKey('z');

            var keyboard = this.game.get('keyboard');
            expect(_.find(keyboard, function(k) { return k.get('value') === 'a'; }).get('isVisible')).toBeTruthy();
            expect(_.find(keyboard, function(k) { return k.get('value') === 'g'; }).get('isVisible')).toBeFalsy();
            expect(_.find(keyboard, function(k) { return k.get('value') === 'z'; }).get('isVisible')).toBeFalsy();
        });

        it('letter is normalized to remove white space and lower case', function() {
            this.game.start("gato");

            this.game.pressKey('   A   ');
            this.game.pressKey('   E   ');

            expect(this.game.get('usedLetters').length).toEqual(1);
            expect(this.game.get('usedLetters')[0]).toEqual('e');
        });

        describe("that is incorrect", function() {
            it('the image is increased', function() {
                this.game.start('gato');

                this.game.pressKey('z');
                expect(this.game.get('image')).toEqual('1.png');

                this.game.pressKey('x');
                expect(this.game.get('image')).toEqual('2.png');

                this.game.pressKey('y');
                expect(this.game.get('image')).toEqual('3.png');

                this.game.pressKey('w');
                expect(this.game.get('image')).toEqual('4.png');

                this.game.pressKey('p');
                expect(this.game.get('image')).toEqual('5.png');
            });

            it("it is added to used letters in lower case", function() {
                this.game.start("gato");

                this.game.pressKey('E');

                expect(this.game.get('usedLetters').length).toEqual(1);
                expect(this.game.get('usedLetters')[0]).toEqual('e');
            });

            it("the game is lost if it is the last chance", function() {
                this.game.start("gato");
                this.game.setAttempts(3);

                this.game.pressKey('x');
                this.game.pressKey('y');
                this.game.pressKey('z');

                expect(this.game.get('state')).toEqual('loose');
                expect(this.game.isOver()).toBeTruthy();
                expect(this.game.get('image')).toEqual('loose.png');
                _.each(this.game.get('letters'), function(l) {
                    expect(l.get('isVisible')).toBeTruthy();
                });
            });

            it("if last change and challenge is selected, marks challenge as lost", function() {
                var challenge = new Hangman.Challenge({ word: 'papa', answered: false });

                this.game.startChallenge(challenge);
                this.game.setAttempts(3);
                this.game.pressKey('x');
                this.game.pressKey('y');
                this.game.pressKey('z');

                expect(challenge.get('answered')).toBeTruthy();
                expect(challenge.get('correct')).toBeFalsy();
            });

            it("should not re-add already pressed letter", function() {
                this.game.start("gato");

                this.game.pressKey('x');
                this.game.pressKey('x');
                expect(this.game.get('usedLetters').length).toEqual(1);
            });

            it("should not accept if game is over", function() {
                this.game.start("papa");

                this.game.pressKey('p');
                this.game.pressKey('a');
                this.game.pressKey('x');

                expect(this.game.get('usedLetters').length).toEqual(0);

            });
        });

        describe("that is correct", function() {
            it("all same letters are revealed", function() {
                this.game.start("casa");
                this.game.pressKey('a');

                expect(this.game.get('letters')[0].get('isVisible')).toBeFalsy();
                expect(this.game.get('letters')[1].get('isVisible')).toBeTruthy();
                expect(this.game.get('letters')[2].get('isVisible')).toBeFalsy();
                expect(this.game.get('letters')[3].get('isVisible')).toBeTruthy();
            });

            it("if last letter, game is won", function() {
                this.game.start("papa");

                this.game.pressKey("p");
                expect(this.game.isOver()).toBeFalsy();

                this.game.pressKey("a");
                expect(this.game.isOver()).toBeTruthy();
                expect(this.game.get('image')).toEqual('win.png');
                expect(this.game.get('state')).toEqual('win');
            });

            it("if last letter and challenge was selected, marks challenge as won", function() {
                var challenge = new Hangman.Challenge({ word: 'papa', answered: false });

                this.game.startChallenge(challenge);
                this.game.pressKey('p');
                this.game.pressKey('a');

                expect(challenge.get('answered')).toBeTruthy();
                expect(challenge.get('correct')).toBeTruthy();
            });
        });
    });
});