﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Hangman.Entities;

namespace Hangman.Models
{
    public class ChallengeViewModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public bool answered { get; set; }
        public bool correct { get; set; }
        public bool isCreator { get; set; }
        public ChallengeResultViewModel[] results { get; set; }

        [Required]
        [DisallowSpaces]
        [Range(1, 15)]
        [RegularExpression("^[A-Za-z]+$")]
        public string word { get; set; }

        public ChallengeViewModel()
        {
        }

        public ChallengeViewModel(Challenge challenge)
        {
            this.id = challenge.Id;
            this.username = challenge.Username;
            this.word = challenge.Word;
            this.results = challenge.Results.Select(r => new ChallengeResultViewModel(r)).ToArray();
        }

    }
}