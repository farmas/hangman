﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangman.Entities;

namespace Hangman.Models
{
    public class ChallengeResultViewModel
    {
        public int id { get; set; }
        public int challengeId { get; set; }

        public string username { get; set; }
        public bool correct { get; set; }

        public ChallengeResultViewModel()
        {

        }

        public ChallengeResultViewModel(ChallengeResult challengeResult)
        {
            this.id = challengeResult.Id;
            this.username = challengeResult.Username;
            this.correct = challengeResult.Correct;
            this.challengeId = challengeResult.Challenge.Id;
        }
    }
}