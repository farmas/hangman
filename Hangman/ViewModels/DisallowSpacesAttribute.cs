﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Hangman.Models
{
    [AttributeUsage(validOn: AttributeTargets.Property)]
    public class DisallowSpacesAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if(value == null){
                return true;
            }


            return !((string)value).Contains(" ");
        }
    }
}
