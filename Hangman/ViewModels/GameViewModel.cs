﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hangman.Models
{
    public class GameViewModel
    {
        public bool IsAuthenticated { get; set; }
        public string UserName { get; set; }
        public string ErrorMessage { get; set; }
    }
}