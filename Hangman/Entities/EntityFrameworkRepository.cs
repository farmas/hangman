﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hangman.Entities
{
    public class EntityFrameworkRepository<T>: IEntityRepository<T>
        where T: class, new()
    {
        private readonly HangmanDbContext _entities;

        public EntityFrameworkRepository(HangmanDbContext entities)
        {
            _entities = entities;
        }

        public void CommitChanges()
        {
            _entities.SaveChanges();
        }

        public T Get(int key)
        {
            return _entities.Set<T>().Find(key);
        }

        public IQueryable<T> GetAll()
        {
            return _entities.Set<T>();
        }

        public void InsertOnCommit(T entity)
        {
            _entities.Set<T>().Add(entity);
        }
    }
}