﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Hangman.Models;

namespace Hangman.Entities
{
    public class HangmanDbContext: DbContext
    {
        public DbSet<Challenge> Challenges { get; set; }
        public DbSet<ChallengeResult> Results { get; set; }
        public DbSet<User> Users { get; set; }

        public HangmanDbContext()
            : base("HangmanDataContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id);

            modelBuilder.Entity<Challenge>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Challenge>()
                .HasMany<ChallengeResult>(c => c.Results)
                .WithRequired(r => r.Challenge);
        }
    }
}