﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hangman.Entities
{
    public class Challenge
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Word { get; set; }
        public virtual ICollection<ChallengeResult> Results { get; set; }

        public Challenge()
        {
            Results = new List<ChallengeResult>();
        }
    }
}