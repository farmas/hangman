﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hangman.Models;
using Hangman.Entities;

namespace Hangman
{
    public interface IEntityRepository<T>
         where T : class, new()
    {
        void CommitChanges();
        IQueryable<T> GetAll();
        void InsertOnCommit(T entity);
    }
}