﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hangman.Entities
{
    public class ChallengeResult
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public bool Correct { get; set; }
        public virtual Challenge Challenge { get; set; }

        public ChallengeResult()
        {
            Challenge = new Challenge();
        }
    }
}