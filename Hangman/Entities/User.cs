﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hangman.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string HashedPassword { get; set; }
        public string Email { get; set; }
        
        public User()
        {
        }

        public User(string username, string hashedPassword, string email)
        {
            this.Username = username;
            this.HashedPassword = hashedPassword;
            this.Email = email;
        }
    }
}