namespace Hangman.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Challenges", "Username", c => c.String());
            AlterColumn("Challenges", "Word", c => c.String());
            AlterColumn("ChallengeResults", "Username", c => c.String());
            AlterColumn("Users", "Username", c => c.String());
            AlterColumn("Users", "HashedPassword", c => c.String());
            AlterColumn("Users", "Email", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("Users", "Email", c => c.String(maxLength: 4000));
            AlterColumn("Users", "HashedPassword", c => c.String(maxLength: 4000));
            AlterColumn("Users", "Username", c => c.String(maxLength: 4000));
            AlterColumn("ChallengeResults", "Username", c => c.String(maxLength: 4000));
            AlterColumn("Challenges", "Word", c => c.String(maxLength: 4000));
            AlterColumn("Challenges", "Username", c => c.String(maxLength: 4000));
        }
    }
}
