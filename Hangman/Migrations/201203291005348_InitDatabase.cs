namespace Hangman.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InitDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Challenges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(maxLength: 4000),
                        Word = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "ChallengeResults",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(maxLength: 4000),
                        Correct = c.Boolean(nullable: false),
                        Challenge_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Challenges", t => t.Challenge_Id, cascadeDelete: true)
                .Index(t => t.Challenge_Id);
            
            CreateTable(
                "Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(maxLength: 4000),
                        HashedPassword = c.String(maxLength: 4000),
                        Email = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("ChallengeResults", new[] { "Challenge_Id" });
            DropForeignKey("ChallengeResults", "Challenge_Id", "Challenges");
            DropTable("Users");
            DropTable("ChallengeResults");
            DropTable("Challenges");
        }
    }
}
