﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangman.Services;
using Hangman.Entities;

namespace Hangman
{
    public class InMemoryUserService: IUserService
    {
        public bool Validate(string username, string password)
        {
            return username == password;
        }

        public string[] GetAllUserEmailAddress()
        {
            return new string[1] { "fede@farmas.net" };
        }

        public IQueryable<Entities.User> GetAll()
        {
            return new User[1] { new User() { Id = 1, Username = "fede", Email = "fede@farmas.net" } }.AsQueryable();
        }

        public User Create(string username, string password, string email)
        {
            throw new NotImplementedException();
        }
    }
}