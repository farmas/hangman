﻿(function (ns, undefined) {
    ns.Server = {
        getWord: function () {
            return $.getJSON(ns.Server.getContextPath() + "Game/Word");
        },
        getContextPath: function () {
            return '/';
        }
    };
})(Hangman);