﻿/// <reference path="lib/jquery-1.5.1.min.js" />
/// <reference path="lib/backbone.js" />
/// <reference path="lib/underscore-min.js" />
(function (ns, undefined) {

    ns.Challenge = Backbone.Model.extend({
        initialize: function () {
            if (this.get('results')) {
                this.results = new ns.ChallengeResults(this.get('results'));
                this.results.setChallengeId(this.get('id'));
                this.canUpdate = true;
            }
            else {
                this.results = new ns.ChallengeResults();
            }
        },
        getResults: function () {
            return this.results;
        },
        addResult: function (result) {
            if (this.canUpdate) {
                this.results.create(result);
            }
        }
    });

    ns.ChallengeResult = Backbone.Model.extend({});

    ns.Letter = Backbone.Model.extend({
        defaults: {
            value: '',
            isVisible: false,
            index: 0
        }
    });

    ns.Game = Backbone.Model.extend({
        defaults: function () {
            return {
                letters: [],
                maxAttempts: 6,
                state: 'play', //win, loose, play
                usedLetters: [],
                keyboard: [],
                captureKeys: false,
                image: '0.png'
            }
        },
        initialize: function () {
            ns.Events.bind('keyPressed', this.pressKey, this);
            this.currentChallenge = new ns.Challenge();

            if (!this.get('challenges')) {
                this.set({ challenges: new ns.Challenges() });
            }
        },
        getResults: function () {
            return this.currentChallenge ? this.currentChallenge.getResults() : new ns.ChallengeResults();
        },
        startChallenge: function (challenge) {
            if (!challenge) {
                challenge = new ns.Challenge();
            }

            // deactive all challenges
            this.get('challenges').deselectAll();

            // activate the new challenge
            this.currentChallenge = challenge;
            this.currentChallenge.set({ active: true });
            this.start(challenge.get('word'));

            if (challenge.get('answered')) {
                this.revealAllLetters(challenge.get('correct'));
            }

            this.trigger('update');
        },
        startRandom: function () {
            var that = this;
            this.currentChallenge.set({ active: false });
            this.currentChallenge = new ns.Challenge();

            ns.Server.getWord().done(function (data) {
                that.start(data.word);
                that.trigger('update');
            });
        },
        start: function (word) {
            var letters = [];
            for (var i = 0, il = word.length; i < il; i++) {
                letters.push(new ns.Letter({
                    value: word[i],
                    index: i
                }));
            }

            var alphabet = 'a b c d e f g h i j k l m n o p q r s t u v w x y z'.split(' ');
            var kb = _.map(alphabet, function (l) {
                return new ns.Letter({
                    value: l,
                    isVisible: true
                });
            });

            this.set({
                letters: letters,
                state: 'play',
                usedLetters: [],
                keyboard: kb,
                captureKeys: true,
                animate: true,
                image: '0.png'
            });
        },
        pressKey: function (key) {
            key = key.trim().toLowerCase();
            if (!this.get('captureKeys') || this.isOver()) {
                return;
            }

            var found = false;

            _.each(this.get('letters'), function (l) {
                if (l.get('value') === key) {
                    l.set({ isVisible: true });
                    found = true;
                }
            });

            if (!found) {
                var usedLetters = this.get('usedLetters');
                if (!_.any(usedLetters, function (l) { return l === key; })) {
                    usedLetters.push(key);
                    this.set({ usedLetters: usedLetters });
                }
            }

            var k = _.find(this.get('keyboard'), function (k) { return k.get('value') === key; });
            if (k) {
                k.set({ isVisible: false });
            }

            this.setState();
            this.trigger('update');
        },
        setAttempts: function (attemptCount) {
            this.set({ maxAttempts: attemptCount });
        },
        setState: function () {
            if (this.get('usedLetters').length >= this.get('maxAttempts')) {
                this.set({ image: 'loose.png' });
                this.revealAllLetters(false);
                this.finishGame(false);
            }
            else if (_.all(this.get('letters'), function (l) { return l.get('isVisible'); })) {
                this.set({ state: 'win', image: 'win.png' });
                this.finishGame(true);
            }
            else {
                this.set({ state: 'play', image: this.get('usedLetters').length + '.png' });
            }
        },
        finishGame: function (correct) {
            this.currentChallenge.set({ answered: true, correct: correct });
            if (!isNaN(this.currentChallenge.get('id'))) {
                this.currentChallenge.addResult({ challengeId: this.currentChallenge.id, correct: correct });
            }
        },
        revealAllLetters: function (correct) {
            var letters = this.get('letters');
            _.each(letters, function (l) {
                l.set({ isVisible: true });
            });

            this.set({
                letters: letters,
                animate: false,
                state: (correct ? 'win' : 'loose'),
                image: this.currentChallenge.get('isCreator') ? '0.png' :
                        correct ? 'win.png' : 'loose.png'
            });
        },
        isOver: function () {
            return this.get('state') !== 'play';
        },
        getCurrentChallenge: function () {
            return this.currentChallenge;
        }
    });

})(Hangman);
