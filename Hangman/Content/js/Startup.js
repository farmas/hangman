﻿(function (ns, undefined) {
    $(function () {
        // create some global events
        // TODO: I don't know where else to put this.
        ns.Events = {};
        _.extend(ns.Events, Backbone.Events);

        // crate challenges collection
        window.challenges = new ns.Challenges();

        // create a game
        window.game = new ns.Game({challenges: window.challenges });

        // create router
        window.router = new ns.HangmanRouter();

        // start the app
        Backbone.history.start();

        // fetch challenges and start the first game
        window.challenges.fetch({
            success: function () {
                window.game.startChallenge(window.challenges.getFirstUnsolved());
            },
            error: function () {
                //TODO: show error message
                window.game.startRandom();
            }
        });

        // setup key handlers
        $('body').keypress(function (event) {
            ns.Events.trigger('keyPressed', String.fromCharCode(event.charCode));
        });

        $('body').on('click', function (event) {
            window.game.set({ captureKeys: false });
        });

        $('.content').on('click', function (event) {
            window.game.set({ captureKeys: true });
            event.stopPropagation();
        });

        // setup custom validators
        $.validator.addMethod("noSpaces", function (value) {
            return value.indexOf(' ') < 0;
        });

        $.validator.addMethod("onlyLetters", function (value) {
            var r = /^[A-Za-z]+$/;
            return r.test(value);
        });
    });
})(Hangman);