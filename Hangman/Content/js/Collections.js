﻿(function (ns, undefined) {

    ns.Challenges = Backbone.Collection.extend({
        model: ns.Challenge,
        url: ns.Server.getContextPath() + "Game/Challenges",
        comparator: function (challenge) {
            return -challenge.id;
        },
        getFirstUnsolved: function () {
            var unsolved = _.find(this.models, function (c) { return c.get('answered') == false });
            return unsolved != undefined ? unsolved : this.models[0];
        },
        addChallenge: function (word) {
            this.create({ word: word, correct: true, answered: true, results: [], isCreator: true });
        },
        deselectAll: function () {
            this.each(function (c) { c.set({ active:false }); });
        }
    });

    ns.ChallengeResults = Backbone.Collection.extend({
        model: ns.ChallengeResult,
        setChallengeId: function (challengeId) {
            this.challengeId = challengeId;
        },
        url: function () {
            return ns.Server.getContextPath() + "Game/ChallengeResults/" + this.challengeId;
        }
    });

})(Hangman);