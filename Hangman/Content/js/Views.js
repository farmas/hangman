﻿(function (ns, undefined) {
    ns.AppView = Backbone.View.extend({
        initialize: function () {
            this.gameView = new ns.GameView({ model: window.game });
            this.challengesView = new ns.ChallengesView({ collection: window.challenges });
        },
        render: function () {
            $('.content').empty().html(this.gameView.render().el);
            $('.sidebar').empty().html(this.challengesView.render().el);
        }
    });

    ns.GameView = Backbone.View.extend({
        className: 'game-container',
        initialize: function () {
            this.template = _.template($('#game-template').html());
            this.model.bind('update', this.render, this);
        },
        render: function () {
            var $el = $(this.el).html(this.template(this.model.toJSON()));

            $('#letters').css('width', (this.model.get('letters').length * 50) + 10);

            var resultsView = new ns.ResultsView({ collection: this.model.getResults() });
            $('#results').html(resultsView.render().el);

            this.animate();
            this.addGameMessage();
            this.setupKeyboard();

            return this;
        },
        addGameMessage: function () {
            var $span = $('<span>');

            if (this.model.getCurrentChallenge().get('isCreator')) {
                $span.text('You are the creator');
            }
            else {
                switch (this.model.get('state')) {
                    case 'win':
                        $span.addClass('success')
                             .text('You Won !');
                        break;
                    case 'loose':
                        $span.addClass('fail')
                             .text('You Lost !');
                        break;
                }
            }

            var $h2 = $('<h2>').append($span);
            $('.game-message').append($h2);
        },
        animate: function () {
            if (!this.model.get('animate')) {
                return;
            }


            this.model.set({ animate: false });
            var timeout = 0;
            $('.letter')
                .hide()
                .each(function () {
                    var l = $(this);
                    setTimeout(function () { l.slideDown(); }, timeout);
                    timeout += 50;
                });

            $('.key')
                .hide()
                .slideDown('fast');
        },
        setupKeyboard: function () {
            var that = this;
            $('.keyboard').on('click', '.key', function (evt) {
                that.model.set({ captureKeys: true });
                that.model.pressKey($(evt.target).text());
            });
        }
    });

    ns.ChallengeView = Backbone.View.extend({
        tagName: 'li',
        events: {
            'click': 'selectChallenge'
        },
        initialize: function () {
            this.template = _.template($('#challenge-template').html());
            this.model.bind('change', this.render, this);
        },
        render: function () {
            var el = $(this.el).removeClass();

            el.addClass(
                this.model.get('isCreator') ? 'creator' :
                !this.model.get('answered') ? 'pending' :
                    this.model.get('correct') ? 'pass' :
                        'fail');

            if (!this.model.get('isCreator')) {
                el.addClass(
                !this.model.get('answered') ? 'pending' :
                    this.model.get('correct') ? 'pass' :
                        'fail');
            }

            if (this.model.get('active')) {
                el.addClass('active');
            }

            el.html(this.template(this.model.toJSON()));
            return this;
        },
        selectChallenge: function (e) {
            window.game.startChallenge(this.model);
            e.stopPropagation();
        }
    });

    ns.ResultsView = Backbone.View.extend({
        tagName: 'ul',
        className: 'status-list',
        initialize: function () {
            this.collection.bind('reset', this.render, this);
            this.collection.bind('add', this.render, this);
        },
        render: function () {
            var $results = $(this.el).html('<h3>Players:</h3>');

            this.collection.each(function (result) {
                $('<li>')
                    .text(result.get('username'))
                    .addClass(result.get('correct') ? 'pass' : 'fail')
                    .appendTo($results);
            });

            return this;
        }
    });

    ns.ChallengesView = Backbone.View.extend({
        tagName: 'div',
        className: 'sideMenu',
        events: {
            'click #playRandom': 'playRandom'
        },
        initialize: function () {
            this.template = _.template($('#challenge-list-template').html());
            this.collection.bind('reset', this.render, this);
            this.collection.bind('add', this.render, this);
        },
        render: function () {
            $(this.el).html(this.template({}));
            var ul = this.$('.challenges');

            if (this.collection.length == 0) {
                $(this.el).find('.spinner').show();
            }
            else {
                $(this.el).find('.spinner').hide();
            }

            this.collection.each(function (challenge) {
                var challengeView = new ns.ChallengeView({ model: challenge });
                ul.append(challengeView.render().el);
            });

            this.$('#newWordForm').validate({
                rules: {
                    'newWordInput': {
                        required: true,
                        noSpaces: true,
                        onlyLetters: true,
                        maxlength: 15
                    }
                },
                messages: {
                    'newWordInput': {
                        required: 'Word cannot be empty.',
                        noSpaces: 'Word cannot have spaces.',
                        maxlength: 'Word must be less than 15 characters.',
                        onlyLetters: 'Word can contain only letters.'
                    }
                }
            });

            var collection = this.collection;
            this.$('#newWordForm').submit(function (event) {
                if ($('#newWordForm').valid()) {
                    var word = $('#newWordInput').val();
                    collection.addChallenge(word);

                    $('#newWordInput').val('');
                    $('#newWordModal').modal('hide');
                }
                event.preventDefault();
            });

            this.$('#newWordModal').on('shown', function () {
                $('#newWordInput').focus();
            });

            return this;
        },
        playRandom: function () {
            window.game.startRandom();
        }
    });

})(Hangman);