(function (ns, undefined) {

    ns.HangmanRouter = Backbone.Router.extend({
        routes: {
            '': 'home'
        },
        home: function () {
            // create and render the app view
            var appView = new ns.AppView({});
            appView.render();
        }
    });
})(Hangman);
