﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangman.Entities;
using System.Web.Helpers;

namespace Hangman.Services
{
    public class UserService: IUserService
    {
        readonly IEntityRepository<User> _usersRepo;
        readonly ICryptoService _cryptoService;

        public UserService(IEntityRepository<User> users, ICryptoService cryptoService)
        {
            _usersRepo = users;
            _cryptoService = cryptoService;
        }
    
        public bool Validate(string username, string password)
        {
            var user = _usersRepo.GetAll().FirstOrDefault(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase));

            if (user != null)

            {
                return _cryptoService.VerifyHashedPassword(user.HashedPassword, password);
            }
            return false;
        }

        public string[] GetAllUserEmailAddress()
        {
            return _usersRepo.GetAll().Select(u => u.Email).ToArray();
        }

        public User Create(string username, string password, string email)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(email))
            {
                throw new InvalidOperationException("username, password and email are required.");
            }

            if (_usersRepo.GetAll().Any(u => u.Username.Equals(username, StringComparison.OrdinalIgnoreCase)))
            {
                throw new InvalidOperationException("username is taken");
            }


            var user = new User(username, _cryptoService.HashPassword(password), email);

            _usersRepo.InsertOnCommit(user);
            _usersRepo.CommitChanges();

            return user;
        }

        public IQueryable<User> GetAll()
        {
            return _usersRepo.GetAll();
        }
    }
}