﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hangman.Entities;

namespace Hangman.Services
{
    public interface IUserService
    {
        IQueryable<User> GetAll();
        User Create(string username, string password, string email);
        bool Validate(string username, string password);
        string[] GetAllUserEmailAddress();
    }
}
