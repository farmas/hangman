﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hangman.Services
{
    public interface ICryptoService
    {
        string HashPassword(string password);
        bool VerifyHashedPassword(string hashedPassword, string password);
    }
}
