﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hangman.Services
{
    public interface IAuthenticationService
    {
        void SetAuthCookie(string username, bool persist);
        string GetUserName();
        bool IsAuthenticated();
        void Logout();
    }
}
