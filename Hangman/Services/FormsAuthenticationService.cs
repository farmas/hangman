﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Hangman.Services
{
    public class FormsAuthenticationService: IAuthenticationService
    {
        public string GetUserName()
        {
            return HttpContext.Current.User.Identity.Name.ToLowerInvariant();
        }

        public bool IsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();            
        }

        public void SetAuthCookie(string username, bool persist)
        {
            FormsAuthentication.SetAuthCookie(username, persist);
        }
    }
}