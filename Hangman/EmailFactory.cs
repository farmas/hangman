﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using Hangman.Models;
using System.Text;
using Hangman.Entities;

namespace Hangman
{
    public class EmailFactory
    {
        public const string FROM_EMAIL = "fede@farmas.net";
        public const string NEW_CHALLENGE_FORMAT_STRING = @"
    <img src='http://hangman.apphb.com/Content/images/hang/0.png' style='float:left;' />
    <div style='padding-top:100px;font-size:larger; font-weight:bold;'>
        {0}
    </div>
    <br />
    <br />
    Visit <a href='http://hangman.apphb.com'>hangman.apphb.com</a> to accept the challenge.";

        public static MailMessage CreateNewChallengeMessage(Challenge model, string[] addresses)
        {
            var letters = new StringBuilder();
            foreach (var l in model.Word)
            {
                letters.Append("___&nbsp;&nbsp;&nbsp;");
            }

            var message = new MailMessage(
                from: FROM_EMAIL,
                to: String.Join(",", addresses),
                subject: String.Format("{0} is challenging you to a word duel!", model.Username),
                body: String.Format(NEW_CHALLENGE_FORMAT_STRING, letters.ToString()));

            message.IsBodyHtml = true;
            return message;
        }
    }
}