﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hangman.Models;
using System.Net.Mail;
using Hangman.Entities;
using Hangman.Services;
using System.Data.Entity;

namespace Hangman.Controllers
{
    public class GameController : Controller
    {
        private static Random _random = new Random();
        private readonly IEntityRepository<Challenge> _challengesRepo;
        private readonly IAuthenticationService _authService;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;

        public GameController(
            IAuthenticationService authService,
            IUserService userService, 
            IEntityRepository<Challenge> challengesRepo, 
            IEmailService emailService)
        {
            this._userService = userService;
            this._challengesRepo = challengesRepo;
            this._authService = authService;
            this._emailService = emailService;
        }

        public ActionResult Index()
        {
            var model = new GameViewModel();
            model.IsAuthenticated = _authService.IsAuthenticated();
            if (_authService.IsAuthenticated())
            {
                model.UserName = _authService.GetUserName();
            }
            
            return View(model);
        }

        [HttpPost]
        public ActionResult Authenticate(string username, string password)
        {
            if (_userService.Validate(username, password))
            {
                _authService.SetAuthCookie(username, true);
                return RedirectToRoute("Default", new { action = "Index" });
            }
            else
            {
                var model = new GameViewModel() { ErrorMessage = "Wrong user or password." };
                return View("Index", model);
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            _authService.Logout();
            return RedirectToRoute("Default", new { action = "Index" });
        }

        [HttpGet]
        public ActionResult Word()
        {
            var challenges = _challengesRepo.GetAll();
            var chosenChallengeIndex = _random.Next(challenges.Count());
            return Json(new {word = challenges.OrderBy(w => w.Id).Skip(chosenChallengeIndex).First().Word}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Challenges(int startIndex = 0, int maxResults = 100)
        {
            var username = _authService.GetUserName();
            var models = new List<ChallengeViewModel>();

            var challenges = _challengesRepo.GetAll()
                                            .Include(c => c.Results)
                                            .OrderBy(c => c.Id).Skip(startIndex).Take(maxResults).ToArray();
            foreach (var challenge in challenges)
            {
                var challengeViewModel = new ChallengeViewModel(challenge);
                if (username.Equals(challenge.Username, StringComparison.OrdinalIgnoreCase))
                {
                    challengeViewModel.answered = challengeViewModel.correct = true;
                    challengeViewModel.isCreator = true;
                }
                else
                {
                    var result = challenge.Results.FirstOrDefault(c => c.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
                    if (result != null)
                    {
                        challengeViewModel.answered = true;
                        challengeViewModel.correct = result.Correct;
                    }
                }
                
                models.Add(challengeViewModel);
            }

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Challenges(ChallengeViewModel challengeViewModel)
        {
            var challenge = new Challenge()
            {
                Username = _authService.GetUserName(),
                Word = challengeViewModel.word.ToLowerInvariant()
            };

            _challengesRepo.InsertOnCommit(challenge);
            _challengesRepo.CommitChanges();

            challengeViewModel.id = challenge.Id;
            challengeViewModel.username = challenge.Username;

            _emailService.Send(EmailFactory.CreateNewChallengeMessage(challenge, _userService.GetAllUserEmailAddress()));

            return Json(challengeViewModel);
        }

        [HttpGet]
        public ActionResult ChallengeResults(int challengeId)
        {
            var results = from r in _challengesRepo.GetAll().Where(c => c.Id == challengeId).First().Results
                          select new ChallengeResultViewModel(r);

            return Json(results.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChallengeResults(ChallengeResultViewModel resultViewModel)
        {
            var user = _authService.GetUserName();
            
            var challenge = _challengesRepo.GetAll().FirstOrDefault(c => c.Id == resultViewModel.challengeId);
            if (challenge == null)
            {
                throw new InvalidOperationException("Challenge was not found.");                
            }
            else if (user.Equals(challenge.Username, StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("User was the creator of this challenge.");                
            }

            var result = challenge.Results.FirstOrDefault(r => r.Username.Equals(user, StringComparison.OrdinalIgnoreCase));

            if (result != null)
            {
                throw new InvalidOperationException("User already solved this challenge.");
            }

            var challengeResult = new ChallengeResult()
            {
                Username = user,
                Correct = resultViewModel.correct,
                Challenge = challenge
            };

            challenge.Results.Add(challengeResult);
            _challengesRepo.CommitChanges();

            resultViewModel.id = challengeResult.Id;
            resultViewModel.username = user;

            return Json(resultViewModel);
        }
    }
}
