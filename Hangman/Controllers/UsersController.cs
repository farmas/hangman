﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hangman.Services;
using Hangman.Entities;
using System.Web.Helpers;

namespace Hangman.Controllers
{
    public class UsersController : Controller
    {
        private const int MAX_USER_COUNT = 4;
        readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        public ActionResult Index()
        {
            var users = _userService.GetAll().ToArray();
            return View(users);
        }

        public ActionResult Create(string username, string password, string email)
        {
            if (_userService.GetAll().Count() >= MAX_USER_COUNT)
            {
                throw new InvalidOperationException("Max user count has been reached");
            }

            var user = _userService.Create(username, password, email);

            return View(user);
        }
    }
}
