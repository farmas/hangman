﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject.Web.Mvc;
using Ninject;

namespace Hangman
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : NinjectHttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{path}.html");

            routes.MapRoute(
                "Users",
                "Users/{action}",
                new { controller = "Users", action = "Index" }

            );

            routes.MapRoute(
                "ChallengeResults", // Route name
                "Game/ChallengeResults/{challengeid}", // URL with parameters
                new { controller = "Game", action = "ChallengeResults" } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}", // URL with parameters
                new { controller = "Game", action = "Index"} // Parameter defaults
            );
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();
            RegisterRoutes(RouteTable.Routes);
        }

        protected override IKernel CreateKernel()
        {
            return new StandardKernel(new HangmanNinjectModule());
        }
    }
}