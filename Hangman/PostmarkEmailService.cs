﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PostmarkDotNet;
using System.Configuration;

namespace Hangman
{
    public class PostmarkEmailService: IEmailService
    {
        public void Send(System.Net.Mail.MailMessage message)
        {
            var postmarkMessage = new PostmarkMessage(message);

            var serverToken = ConfigurationManager.AppSettings["POSTMARK_SERVER_TOKEN"];
            var client = new PostmarkClient(serverToken);
            client.SendMessage(postmarkMessage);
        }
    }
}