﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using Hangman.Models;
using System.Configuration;
using Hangman.Services;
using Hangman.Entities;

namespace Hangman
{
    public class HangmanNinjectModule: NinjectModule
    {
        public override void Load()
        {
            if (ConfigurationManager.AppSettings["Environment"].Equals("Debug"))
            {
                Bind<IEmailService>()
                    .To<InMemoryEmailService>();

                Bind<IUserService>()
                    .To<InMemoryUserService>()
                    .InRequestScope();
            }
            else
            {
                Bind<IEmailService>()
                    .To<PostmarkEmailService>()
                    .InRequestScope();
                
                Bind<IUserService>()
                    .To<UserService>()
                    .InRequestScope();
            }

            Bind<IEntityRepository<User>>()
                   .To<EntityFrameworkRepository<User>>()
                   .InRequestScope();

            Bind<IEntityRepository<Challenge>>()
                .To<EntityFrameworkRepository<Challenge>>()
                .InRequestScope();

            Bind<ICryptoService>()
                .To<CryptoService>()
                .InRequestScope();

            Bind<IAuthenticationService>()
                .To<FormsAuthenticationService>()
                .InRequestScope();
        }
    }
}